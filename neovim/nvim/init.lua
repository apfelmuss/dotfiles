--
-- native neovim settings
-- - options
-- - commands
-- - keymaps
-- - autocmds
--
require( 'config' )

--
-- plugin section
--

-- clone lazy to user-specific data (default: $HOME/.local/share)
local lazypath = vim.fn.stdpath( 'data' ) .. '/lazy/lazy.nvim'
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end

-- add lazy dir to runtimepath in front of other paths
vim.opt.runtimepath:prepend( lazypath )

-- configure lazy and and get the plugin specs from 'plugins' module
require( 'lazy' ).setup( 'plugins' )

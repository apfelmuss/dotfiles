return {
    'nvim-neo-tree/neo-tree.nvim',

    branch = "v3.x",

    enabled = false,

    lazy = false,
    priority = 1000,

    dependencies = {
        "nvim-lua/plenary.nvim",
        "MunifTanjim/nui.nvim",
        "nvim-tree/nvim-web-devicons",
    },

    opts = {},

    config = function( _, opts )
        require( 'neo-tree').setup( opts )
    end
}

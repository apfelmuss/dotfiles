return {
    'dominikduda/vim_current_word',

    enabled = true,

    lazy = false,
    priority = 1000,

    config = function()

        -- Enable/disable plugin
        vim.g['vim_current_word#enabled'] = 1

        -- Twins of word under cursor:
        vim.g['vim_current_word#highlight_twins'] = 1

        -- The word under cursor:
        vim.g['vim_current_word#highlight_current_word'] = 1

        -- Setting this option to more than 0 will enable delayed highlighting. The
        -- value of this variable is a delay in milliseconds.
        -- let g:vim_current_word#highlight_delay = 0o

        -- Disabling this option will make the word highlight persist over window
        -- switches and even over focusing different application window.
        -- let g:vim_current_word#highlight_only_in_focused_window = 1

        -- To avoid specific filetypes, add this variable to your vimrc:
        -- let g:vim_current_word#excluded_filetypes = ['ruby']

        -- To prevent the plugin from running in one or more buffers add following to
        -- your vimrc:
        -- The example below disables the plugin in:
        -- - Every buffer which name start with NERD_tree_
        -- - Every buffer which name equals your_buffer_name.rb
        -- - Every buffer which name ends with .js
        -- autocmd BufAdd NERD_tree_*,your_buffer_name.rb,*.js :let b:vim_current_word_disabled_in_this_buffer = 1

        -- Change highlight style of twins of word under cursor:
        -- hi CurrentWordTwins guifg=#XXXXXX guibg=#XXXXXX gui=underline,bold,italic ctermfg=XXX ctermbg=XXX cterm=underline,bold,italic
        --                            └┴┴┴┴┤        └┴┴┴┴┤     └┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┤         └┴┤         └┴┤       └┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┤
        --      gui-vim font color hex code│             │   gui-vim special styles│           │           │ console-vim special styles│
        --      ───────────────────────────┘             │   ──────────────────────┘           │           │ ──────────────────────────┘
        --              gui-vim background color hex code│     console-vim font term color code│           │
        --              ─────────────────────────────────┘     ────────────────────────────────┘           │
        --                                                           console-vim background term color code│
        --                                                           ──────────────────────────────────────┘
        --
        -- Change highlight style of the word under cursor:
        -- hi CurrentWord guifg=#XXXXXX guibg=#XXXXXX gui=underline,bold,italic ctermfg=XXX ctermbg=XXX cterm=underline,bold,italic
        --                       └┴┴┴┴┴──┐     └┴┴┴┴┤     └┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┤         └┴┤         └┴┤       └┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┤
        --    gui-vim font color hex code│          │   gui-vim special styles│           │           │ console-vim special styles│
        --    ───────────────────────────┘          │   ──────────────────────┘           │           │ ──────────────────────────┘
        --         gui-vim background color hex code│     console-vim font term color code│           │
        --         ─────────────────────────────────┘     ────────────────────────────────┘           │
        --                                                      console-vim background term color code│
        --                                                      ──────────────────────────────────────┘--

        -- Example
        -- hi CurrentWord ctermbg=53
        -- hi CurrentWordTwins ctermbg=237

        -- Just underline word under cursor to avoid highlighting
        -- as it would be using * or #
        vim.cmd.highlight( 'CurrentWord gui=underline cterm=underline' )

    end
}

return {
    'preservim/tagbar',

    enabled = true,

    lazy = false,
    priority = 1000,

    config = function()

        vim.keymap.set( 'n', '<C-c>', '<Cmd>TagbarToggle<CR>' )

        -- sort tags in according to their order in source file
        vim.g['tagbar_sort'] = 0

    end,
}

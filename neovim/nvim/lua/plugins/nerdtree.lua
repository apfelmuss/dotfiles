return {
    'preservim/nerdtree',

    enabled = true,

    lazy = false,
    priority = 1000,

    config = function()

        vim.keymap.set( 'n', '<C-n>', '<Cmd>NERDTreeToggle<CR>' )

        -- Close the tab if NERDTree is the only window remaining in it.
        vim.api.nvim_create_autocmd({ 'BufEnter' }, {
            pattern = '*',
            command = [[
                if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
            ]],
        })

    end,
}

return {
    'nvim-treesitter/nvim-treesitter',

    enabled = false,

    lazy = false,
    priority = 1000,

    opts = {},

    config = function( _, opts )
        require( 'nvim-treesitter' ).setup( opts )
    end
}

return {
    'akinsho/bufferline.nvim',

    enabled = true,

    lazy = false,
    priority = 1000,

    dependencies = { 'nvim-tree/nvim-web-devicons' },

    opts = {
        numbers = 'buffer_id',
        indicator = { icon = '', style = 'none' },
        left_trunc_marker = '',
        right_trunc_marker = '',
        modified_icon = '●',
        show_buffer_icons = false,
        show_buffer_close_icons = false,
        show_close_icon = false,
        show_tab_indicators = true,
        separator_style = 'slant',
        always_show_bufferline = true,
    },

    config = function( _, opts )
        require( 'bufferline' ).setup( { options = opts } )
    end,
}

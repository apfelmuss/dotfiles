return {
    'lewis6991/gitsigns.nvim',

    enabled = true,

    event = { "BufReadPre", "BufNewFile" },

    opts = {
        signs = {
            add          = { text = '+' },
            change       = { text = '~' },
            delete       = { text = '_' },
            topdelete    = { text = '‾' },
            changedelete = { text = '~' },
            untracked    = { text = '┆' },
        },
        signcolumn = true,  -- Toggle with `:Gitsigns toggle_signs`
        numhl      = false, -- Toggle with `:Gitsigns toggle_numhl`
        linehl     = false, -- Toggle with `:Gitsigns toggle_linehl`
        word_diff  = false, -- Toggle with `:Gitsigns toggle_word_diff`
        watch_gitdir = {
            follow_files = true
        },
        auto_attach = true,
        attach_to_untracked = false,
        current_line_blame = false, -- Toggle with `:Gitsigns toggle_current_line_blame`
        current_line_blame_opts = {
            virt_text = true,
            virt_text_pos = 'eol', -- 'eol' | 'overlay' | 'right_align'
            delay = 1000,
            ignore_whitespace = false,
            virt_text_priority = 100,
        },
        current_line_blame_formatter = '<author>, <author_time:%Y-%m-%d> - <summary>',
        current_line_blame_formatter_opts = {
            relative_time = false,
        },
        sign_priority = 6,
        update_debounce = 100,
        status_formatter = nil, -- Use default
        max_file_length = 40000, -- Disable if file is longer than this (in lines)
        preview_config = {
            -- Options passed to nvim_open_win
            border = 'single',
            style = 'minimal',
            relative = 'cursor',
            row = 0,
            col = 1
        },
    },

    keys = {

        -- 'Go To Hunk' keymaps like 'Go To Buffer' or 'Go To Tab'
        { 'gh', '<Cmd>Gitsigns next_hunk<CR>', desc = 'Go to next hunk' },
        { 'gH', '<Cmd>Gitsigns prev_hunk<CR>', desc = 'Go to previous hunk' },

        --
        -- Leader keymaps with prefix 'g' for 'git':
        --

        -- git hunk
        { '<Leader>gh', '<Cmd>Gitsigns next_hunk<CR>', desc = 'Next hunk' },
        { '<Leader>gH', '<Cmd>Gitsigns prev_hunk<CR>', desc = 'Previous hunk' },

        -- git stage
        { '<Leader>gs', '<Cmd>Gitsigns stage_hunk<CR>', desc = 'Stage hunk' },
        -- git reset
        { '<Leader>gr', '<Cmd>Gitsigns reset_hunk<CR>', desc = 'Reset hunk' },
        -- git undo
        { '<Leader>gu', '<Cmd>Gitsigns undo_stage_hunk<CR>', desc = 'Undo stage hunk' },

        -- git blame
        { '<Leader>gbs', '<Cmd>Gitsigns blame_line<CR>', desc = 'Blame line (short)' },
        -- git diff
        { '<Leader>gd', '<Cmd>Gitsigns diffthis<CR>', desc = 'Diff this' },
    },

    config = function( _, opts )

        require( 'gitsigns' ).setup( opts )

        local gs = package.loaded.gitsigns

        --
        -- More Leaderkey maps with prefix 'g' for 'git':
        --

        -- git stage
        vim.keymap.set(
            'v',
            '<Leader>gs',
            function()
                gs.stage_hunk( { vim.fn.line( '.' ), vim.fn.line( 'v' ) } )
            end,
            { desc = "Stage hunk" }
        )

        -- git reset
        vim.keymap.set(
            'v',
            '<Leader>gr',
            function()
                gs.reset_hunk( { vim.fn.line( '.' ), vim.fn.line( 'v' ) } )
            end,
            { desc = "Reset hunk" }
        )

        -- git blame
        vim.keymap.set(
            'n',
            '<Leader>gbf',
            function()
                gs.blame_line( { full = true } )
            end,
            { desc = "Blame line (full)" }
        )

    end,
}

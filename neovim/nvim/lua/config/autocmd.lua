-- reomve all trailing spaces when buffer is written
vim.api.nvim_create_autocmd({ 'BufWritePre' }, {
    pattern = '*',
    command = [[%s/\s\+$//e]],
})

-- Quickfix specials:
-- 1. The quickfix buffer shall not be accessable with :bn or :bp.
--    Therefore remove it from buffer list.
-- 2. If the qickfix window is opend it shall be placed as the very bottom
--    split.
vim.api.nvim_create_autocmd({ 'FileType' }, {
    pattern = 'qf',
    callback = function()
        vim.opt.buflisted = false
        vim.cmd( 'wincmd J')
    end,
})

-- Use <Space> as <Leader> ...
vim.g.mapleader = ' '
-- ... and <Bslash> as <LocalLeader>
vim.g.maplocalleader = '\\'

-- simple move between splits. just hit CTRL+[hjkl]
vim.keymap.set( 'n', '<C-j>', '<C-W><C-j>', { desc = "Got to the down window" } )
vim.keymap.set( 'n', '<C-k>', '<C-W><C-k>', { desc = "Got to the up window" } )
vim.keymap.set( 'n', '<C-l>', '<C-W><C-l>', { desc = "Got to the right window" } )
vim.keymap.set( 'n', '<C-h>', '<C-W><C-h>', { desc = "Got to the left window" } )

-- added new short cuts 'gb' and 'gB' to switch through the buffers
-- like 'gt' and 'gT' to move through tabs
vim.keymap.set( 'n', 'gb', '<Cmd>bnext<CR>', { desc = "Go to next buffer" } )
vim.keymap.set( 'n', 'gB', '<Cmd>bprevious<CR>', { desc = "Go to previous buffer" } )
